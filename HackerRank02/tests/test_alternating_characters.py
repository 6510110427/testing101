
import unittest
from code02.AlternatingCharacters import alternatingCharacters

class AlternatingCharactersTest(unittest.TestCase):
    
    def test_alternatingdetect_1(self):
        string = 'AAAA'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(3,alternatingtest)
    
    def test_alternatingdetect_2(self):
        string = 'BBBBB'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(4,alternatingtest)
    
    def test_alternatingdetect_3(self):
        string = 'ABABABAB'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(0,alternatingtest)

    def test_alternatingdetect_4(self):
        string = 'BABABA'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(0,alternatingtest)

    def test_alternatingdetect_5(self):
        string = 'AAABBB'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(4,alternatingtest)
    
if __name__=="__main__":
    unittest.main()