def funnyString(s):
    
    r = s[::-1]
    ascii_s = []
    ascii_r = []
    isitfunnys = []
    isitfunnyr = []
    for index in range(len(s)):
        ascii_s.append(ord(s[index]))
        ascii_r.append(ord(r[index]))
    
    for i in range(len(ascii_s)-1):
            isitfunnys.append(abs(ascii_s[i]-ascii_s[i+1]))
            isitfunnyr.append(abs(ascii_r[i]-ascii_r[i+1]))
    
    if isitfunnys == isitfunnyr:
        return 'Funny'
    else:
        return 'Not Funny'