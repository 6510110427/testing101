import unittest
from code05.gridchallenge import gridChallenge

class GridChallengeTest(unittest.TestCase):
    
    def test_gridchallenge_1(self):
        gridlist = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        gridtest = gridChallenge(gridlist)
        self.assertEqual('YES',gridtest)
    
    def test_gridchallenge_2(self):
        gridlist = ['abc', 'lmp', 'qrt']
        gridtest = gridChallenge(gridlist)
        self.assertEqual('YES',gridtest)
    
    def test_gridchallenge_3(self):
        gridlist = ['mpxz', 'abcd', 'wlmf']
        gridtest = gridChallenge(gridlist)
        self.assertEqual('NO',gridtest)
    
    def test_gridchallenge_4(self):
        gridlist = ['abc', 'hjk', 'mpq', 'rtv']
        gridtest = gridChallenge(gridlist)
        self.assertEqual('YES',gridtest)
    
if __name__=="__main__":
    unittest.main()