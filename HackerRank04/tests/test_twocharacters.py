import unittest
from code04.twocharacters import alternate

class TwoCharactersTest(unittest.TestCase):
    
    def test_two_char_alternating_1(self):
        string = 'beabeefeab'
        two_char_alternating = alternate(string)
        self.assertEqual(5,two_char_alternating)
    
    def test_two_char_alternating_2(self):
        string = 'asdcbsdcagfsdbgdfanfghbsfdab'
        two_char_alternating = alternate(string)
        self.assertEqual(8,two_char_alternating)
    
    def test_two_char_alternating_3(self):
        string = 'asvkugfiugsalddlasguifgukvsa'
        two_char_alternating = alternate(string)
        self.assertEqual(0,two_char_alternating)
    
    
if __name__=="__main__":
    unittest.main()