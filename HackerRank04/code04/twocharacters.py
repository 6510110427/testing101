def alternate(s):
    # Write your code here
    char_list = sorted(list(set(s)))
    pair_list = []
    for i in range(len(char_list)-1):
        for j in range(i+1,len(char_list)):
            pair_list.append(sorted([char_list[i],char_list[j]]))
    
    check_list = []
    individual_list = []
    for pair in pair_list:
        for char in s:
            if char == pair[0] or char == pair[1]:
                individual_list.append(char)
            else:
                continue
        check_list.append(individual_list)
        individual_list = []
    
    findlongest_list = []
    for slist in check_list:
        individual_list = [slist[0]]
        for l in range(1,len(slist)):
                if slist[l] != individual_list[-1]:
                    individual_list.append(slist[l])
                else:
                    individual_list.clear()
                    break
        findlongest_list.append(individual_list)
    longest = 0
    for m in findlongest_list:
        if len(m) > longest:
            longest = len(m)
    
    if longest != 0:
        return longest
    else :
        return 0



